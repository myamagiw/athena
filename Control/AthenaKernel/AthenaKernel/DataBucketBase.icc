/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file  AthenaKernel/DataBucketBase.icc
 * @author scott snyder
 * @date Nov 2005
 * @brief A non-templated base class for DataBucket, allows to access the
 *        transient object address as a void*
 *        Implementation file.
 */

#include "AthenaKernel/ClassID_traits.h"
#include <type_traits>
#include <typeinfo>


/**
 * @brief Return the contents of the @c DataBucket,
 *        converted to type @a T.  Note that only
 *        derived->base conversions are allowed here.
 *        @a T must have a valid Class ID for this to work.
 * @param irt To be called if we make a new instance.
 * @param isConst True if the object being converted is regarded as const.
 */
template <class T>
T* DataBucketBase::cast (SG::IRegisterTransient* irt /*= 0*/,
                         bool isConst /*= true*/)
{
  return reinterpret_cast<T*>
    (cast (typeid (typename std::remove_const<T>::type), irt, isConst));
}

